import math

class Graph:

    def __init__(self, V: int):
        self.V = V
        self.adj = [[]] * V

    def addEdge(self, node_u : int, node_v : int, weight : int):
        self.adj[node_u].append((node_v, weight))
        self.adj[node_v].append((node_u, weight))

    def shortestPath(self, source : int, max_weight : int):
        dist = [(int, [])] * self.V

        for i in range(0, self.V):
            dist[i][0] = math.inf

        B = [[]] * (max_weight * self.V + 1)

        B[0].append(source)
        dist[source][0] = 0

        idx = 0

        while True:
            while len(B[idx]) == 0 and idx < (max_weight * self.V):
                idx += 1

            if idx == (max_weight * self.V):
                break

            u = B[idx].pop()

            i = self.adj[u].b

def main():
    print('hello world')
    V = 9

    g = Graph(V)

    g.addEdge(0, 1, 4)
    g.addEdge(0, 7, 8)
    g.addEdge(1, 2, 8)
    g.addEdge(1, 7, 11)
    g.addEdge(2, 3, 7)
    g.addEdge(2, 8, 2)
    g.addEdge(2, 5, 4)
    g.addEdge(3, 4, 9)
    g.addEdge(3, 5, 14)
    g.addEdge(4, 5, 10)
    g.addEdge(5, 6, 2)
    g.addEdge(6, 7, 1)
    g.addEdge(6, 8, 6)
    g.addEdge(7, 8, 7)

    # maximum weighted edge - 14
    g.shortestPath(0, 14)


if __name__ == "__main__":
    main()